package main

import (
	"fmt"
	AliYunOssClient "gitee.com/tym_hmm/oss-aliyun-client"
	"strings"
	"testing"
)

func TestUpLoad(t *testing.T)  {

	str:="aaaa"
	reader:=strings.NewReader(str)
	client:=AliYunOssClient.NewAliYunOssClient("xxx", "xxx", "xx")

	err :=client.PutStream("yuanyz", "a.txt",reader )
	if err !=nil{
		fmt.Println(err)
	}
}

/**
分片上传测试
 */
func TestChunkUpLoad(t *testing.T)  {
	client:=AliYunOssClient.NewAliYunOssClient("xxx", "xxx", "xxx")
	//bucketName string, filePath string, fileName string, chunkNum int
	err:=client.PutStreamChunk("fn-backup", "./xxx.txt", "xxx.txt", 3)
	if err !=nil{
		fmt.Println(err)
	}
}