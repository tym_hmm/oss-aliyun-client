package AliYunOssClient

import "io"

type AliYunClientInterface interface {
	//文件上传(本地文件上传)
	Put(bucket, fileName, localFilePath string) error

	/**
	字符串上传
	*/
	PutStream(bucketName string, fileName string, reader io.Reader) error

	/**
	分片上传
	*/
	PutStreamChunk(bucketName string, filePath string, fileName string, chunkNum int) error
}
