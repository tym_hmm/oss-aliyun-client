package AliYunOssClient

import (
	"crypto/sha256"
	"encoding/hex"
)

/**
生成hash256
 */
func Hash256(str string) string {
	h := sha256.New()
	h.Write([]byte(str))
	sum := h.Sum(nil)
	s := hex.EncodeToString(sum)
	return s
}